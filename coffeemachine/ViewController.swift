//
//  ViewController.swift
//  coffeemachine
//
//  Created by V.K. on 2/11/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import UIKit

var coffeeShop = CoffeeMachine(title: "Central Perk")

class ViewController: UIViewController {
    
//    var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
                
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        machineName.text = coffeeShop.machineName
        updateLevelLabels()
        
//        timer = Timer.scheduledTimer(
//            timeInterval: 1,
//            target: self,
//            selector: #selector(self.updateLevelLabels),
//            userInfo: nil,
//            repeats: true
//        )
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        timer?.invalidate()
    }
    
    @IBOutlet weak var coffeeStock: UILabel!
        
    @IBOutlet weak var waterStock: UILabel!
    
    @IBOutlet weak var milkStock: UILabel!
    
    @IBOutlet weak var machineName: UILabel!
        
    @IBOutlet weak var messageBoard: UILabel!
    
    @IBOutlet var serveDrinkButtons: [UIButton]!
    
    let buttonDrinkTypes: [DrinkType] = [
        .espresso,
        .americano,
        .latte,
        .flat_white
    ]
    
    @IBAction func serveADrink(_ sender: UIButton) {
        if let numDrinkButton = serveDrinkButtons.firstIndex(of: sender) {
            outputMessageInColor(message: coffeeShop.serveDrink(name: buttonDrinkTypes[numDrinkButton]))
        }
    }
    
    @IBOutlet var addComponentButtons: [UIButton]!
    
    let componentIncrements: [(type: Component, amount: Int)] = [
        (type: .coffee, amount: 100),
        (type: .water, amount: 500),
        (type: .milk, amount: 200)
    ]
    
    @IBAction func addComponent(_ sender: UIButton) {
        if let numComponentButton = addComponentButtons.firstIndex(of: sender) {
            coffeeShop.addComponent(componentIncrements[numComponentButton])
            let buttonTitle = componentIncrements[numComponentButton].type
            print("Added \(componentIncrements[numComponentButton].amount) to \(buttonTitle) bin.")
            updateLevelLabels()
        }
    }
    
    func updateLevelLabels() {
        coffeeStock.text = "\(coffeeShop.getLevel(type: .coffee))"
        waterStock.text = "\(coffeeShop.getLevel(type: .water))"
        milkStock.text = "\(coffeeShop.getLevel(type: .milk))"
        
    }
    
    func outputMessageInColor(message: (Bool, String)) {
        // message.0 - delivery success, message.1 - message string
        print("\(coffeeShop.machineName): \(message.1)")
        messageBoard.textColor = (message.0) ? .black : .red
        messageBoard.text = message.1
        updateLevelLabels()
    }
}


