//
//  CoffeeMachineClass.swift
//  coffeemachine
//
//  Created by V.K. on 2/11/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import Foundation

enum Component {
    case coffee, water, milk
}

enum DrinkType: String {
    case espresso, americano, latte, flat_white
}

class Drink {
    let name: DrinkType
    let madeFrom: [Component:Int]
    var servable: Bool
    init(name: DrinkType) {
        self.name = name
        self.servable = false
        switch name {
        case .espresso:
            self.madeFrom = [.coffee:20, .water:50, .milk:0]
        case .americano:
            self.madeFrom = [.coffee:10, .water:100, .milk:0]
        case .latte:
            self.madeFrom = [.coffee:20, .water:50, .milk:150]
        case .flat_white:
            self.madeFrom = [.coffee:20, .water:50, .milk:100]
        }
    }
}

class CoffeeMachine {
    var components: [Component:Int]
    var drinkMenu: [Drink]
    let machineName: String
    
    init(title: String) {
        self.machineName = title
        self.components = [.coffee:0, .water:0, .milk:0]
        self.drinkMenu = [
            Drink(name: .espresso),
            Drink(name: .americano),
            Drink(name: .latte)
        ]
    }
    
    func getLevel(type: Component) -> Int {
        return components[type] ?? 0
    }
    
    func addComponent(_ increment: (type: Component, amount: Int)) {
        components[increment.type]! += increment.amount
    }
    
    func serveDrink(name: DrinkType) -> (Bool, String) {
        // check availability
        let menuSelection = drinkMenu.firstIndex(where: { $0.name == name })
        // try to make
        if menuSelection != nil {
            let prepared = makeDrink(menuNum: menuSelection!)
            if !prepared {
                return (false, "Check component levels")
            }
        } else {
            return (false, "Not on menu today, sorry")
        }
        return (true, "Here is \(name.rawValue). Enjoy it!")
    }
    
    private func makeDrink(menuNum: Int) -> Bool {
        var compAvailable = true
        for component in components.keys {
            let compEnough: Bool = components[component]! >= drinkMenu[menuNum].madeFrom[component]!
            compAvailable = compAvailable && compEnough
            
        }
        if !compAvailable {
            return false
        }
        for component in components.keys {
            components[component]! -= drinkMenu[menuNum].madeFrom[component]!
        }
        return true
    }
}
